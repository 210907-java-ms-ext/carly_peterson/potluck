# Potluck

A social web app for the avid chef

## Branches
- user-registration
- user-login
- recipe-add
- recipe-edit
- recipe-view
- recipe-view-all
- recipe-delete

## Remote Technology Information
- Tomcat: ec2-3-140-238-125.us-east-2.compute.amazonaws.com:8090
- Secure IP's
    - Weaver: 173.79.133.77/32
    - Ledesma: 99.147.128.14/32
    - Abdhir: 76.126.43.235/32

## Technologies
- Java 8
- PostgreSQL
- Hibernate
- Spring
    - Core
    - MVC
- Angular
- DevOps Pipeline
    - Jenkins
    - Maven
    - Hosted on EC2
    - Running JUnit tests
    - Deploying to Tomcat
    - Updates to Slack

## User Stories
### MVP
- [x] As a user I can register for a new account
- [x] As a user I can log in to my own account
- [x] As a user I can view my recipes
- [x] As a user I can make a recipe in my account
- [x] As a user I can edit my recipes
- [x] As a user I can delete one of my recipes
- [x] As a user I can view all recipes

### User Stories Division
##### Peterson
- [x] As a user I can register for a new account
- [x] As a user I can log in to my own account

##### Weaver
- [x] As a user I can edit my recipes
- [x] As a user I can delete one of my recipes

##### Ledesma
- [x] As a user I can view my recipes
- [x] As a user I can view all recipes

#### Abdhir
- [x] As a user I can make a recipe in my account
